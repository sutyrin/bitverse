from pycsp.greenlets import *

class Ticker:
    def __init__(self):
        self.objects = []
        self.tick_number = 0

    def addObject(self, o):
        self.objects.append(o)

    def tick(self):
        self.tick_number += 1
        for o in self.objects:
            o.tick()
    
    def is_registered(self, o):
        return o in self.objects
    
    def del_object(self, o):
        if o in self.objects:
            self.objects.remove(o)

class PTicker(Ticker):
    def __init__(self):
        Ticker.__init__(self)
        self.chans = []
    
    def run(self):
        while True:
            self.tick()
            
    def addObject(self, o):
        chan = Channel()
        o.ticker_input = +chan
        #self.chans[
        
    def tick(self):
        for chan in self.chans:
            chan(None)
    
    def del_object(self, o):
        pass
         
    

class Keeper:
    def __init__(self):
        self.objects = dict()
        pass
    
    def add_object(self, o):
        self.objects[o.get_name()] = o

    def del_object_by_name(self, name):
        if name in self.objects:
            del self.objects[name]
            
    def get_object_by_name(self, name):
        if name in self.objects:
            return self.objects[name]
        else:
            return None
        
    def get_object_by_class(self, a_class):
        for name in self.objects:
            if isinstance(self.objects[name], a_class):
                return self.objects[name]
            
class Tickable:
    def __init__(self):
        global getGlobalTicker
        getGlobalTicker().addObject(self)
    def tick(self):
        pass    
    def destroy(self):
        global getGlobalTicker
        getGlobalTicker().del_object(self)

def reset():
    global ticker, keeper
    ticker = Ticker()
    keeper = Keeper()
    
def getGlobalTicker():
    global ticker
    return ticker

def getGlobalKeeper():
    global keeper
    return keeper
    
reset()
