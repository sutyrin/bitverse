from elements import *
from keepers import Tickable

class TinyMachine:

    def __init__(self):
        self.memory = Memory(address_width=1, cell_width=1)

class ThreeAddressMachine:
    def __init__(self, memory=None):
        self.memory = memory
        self.ports = {0: Port(), 1: Port()}
        self.reset()

    def int_from_imemory(self, address):
        return self.ibits2int(self.memory[address])

    def int_from_memory(self, address):
        return self.bits2int(self.memory[address])

    def int_to_memory(self, address, value):
        self.memory[address] = self.int2value(value)

    def bits2int(self, value):
        return base2int(value, self.memory.address_base)

    def ibits2int(self, value):
        return ibase2int(value, self.memory.address_base)

    def int2value(self, value):
        return int2base(value, self.memory.address_base,
                               self.memory.cell_width)

    def int2address(self, value):
        return int2base(value, self.memory.address_base,
                               self.memory.address_width)

    def op_halt(self, ops):
        self.halted = True

    def perform_arithmetic(self, method, ops):
        op1 = self.int_from_memory(ops[0])
        op2 = self.int_from_memory(ops[1])
        self.int_to_memory(ops[2], op1.__getattribute__(method)(op2))

    def op_add(self, ops):
        self.perform_arithmetic('__add__', ops)

    def op_sub(self, ops):
        self.perform_arithmetic('__sub__', ops)

    def op_inport(self, ops):
        port_index = 0  # self.bits2int(ops[0])
        op = self.ports[port_index].read_value()
        self.memory[ops[2]] = op

    def op_outport(self, ops):
        port_index = 1  # self.bits2int(ops[2])
        op = self.memory[ops[0]]
        self.ports[port_index].write_value(op)

    def step_to_halt(self):
        while not self.halted:
            self.step()

    def step(self, n=1):
        if not self.halted:
            while n > 0:
                n -= 1
                self.current_command += 1
                # print self.memory
                # print 'cmd addr: ', self.int2address(self.current_command)
                cmd = self.memory[self.int2address(self.current_command)]
                opcode = cmd[:self.__class__.opcode_width]
                ops = cmd[self.__class__.opcode_width:]
                # print 'cmd: ', cmd
                # print 'opcode: ', opcode
                ops_list = []
                while ops:
                    operand = ops[:self.memory.address_width ]
                    ops = ops[ self.memory.address_width:]
                    ops_list.append(operand)
                op_func = getattr(self, self.__class__.op_functions[opcode])
                if op_func:
                  op_func(ops_list)

    def op_je(self, ops):
        op1 = self.int_from_memory(ops[0])
        op2 = self.int_from_memory(ops[1])
        if op1 == op2:
            self.set_current_command_from_bits(ops[2])

    def op_jne(self, ops):
        op1 = self.int_from_memory(ops[0])
        op2 = self.int_from_memory(ops[1])
        if op1 <> op2:
            self.set_current_command_from_bits(ops[2])

    def set_current_command_from_bits(self, value):
        self.current_command = self.bits2int(value) - 1

    def op_jmp(self, ops):
        self.set_current_command_from_bits(ops[2])

    def reset(self):
        self.current_command = -1
        self.halted = False
        # self.error = False

    opcode_width = 3
    op_functions = {
                    '001' : 'op_add',
                    '010' : 'op_sub',
                    '011' : 'op_jne',
                    '100' : 'op_inport',
                    '101' : 'op_outport',
                    '110' : 'op_jmp',
                    '111' : 'op_halt',
                    }

class SimpleMachine(Tickable):
    def __init__(self):
        Tickable.__init__(self)
        self.name = 'm'
        self.generator = SolarBattery(2)
        self.port = Port()
        self.port.generator = self.generator
        self.cell = Cell()
        self.cell.generator = self.generator
        self.bus = Bus(self.port, self.cell)
        self.bus.generator = self.generator

        self.connected_machine = None

    def __repr__(self):
        return 'machine \'%s\':\n  %s\n  %s\n  %s' % \
                (self.name, self.port, self.cell, self.bus)

    def cmd_cell_to_port(self):
        self.bus.send_2to1()

    def cmd_port_to_cell(self):
        self.bus.send_1to2()

    def connect_to(self, other_machine, connect_other_part=True):
        self.connected_machine = other_machine
        if connect_other_part:
            self.connected_machine.connect_to(self, False)
        self.port.plug_with_port(other_machine.port)

    def disconnect(self, disconnect_that_part=True):
        if disconnect_that_part:
            self.connected_machine.disconnect(False)
        self.port.unplug()

    def recieve_datum(self):
        self.connected_machine.set_datum_on_port()
        self.cmd_port_to_cell()

    def set_datum_on_port(self):
        self.cmd_cell_to_port()

    def transmit_datum(self):
        self.connected_machine.recieve_datum()

class UM3(ThreeAddressMachine):
    def __init__(self):
        self.address_base = 16
        self.address_width = 4
        self.opcode_width = 2
        ThreeAddressMachine.__init__(self,
             Memory(address_width=self.address_width,
                 cell_width=self.opcode_width + 3 * self.address_width,
                 address_base=self.address_base))

    def op_mov(self, ops):
        # print ops
        self.memory[ops[2]] = self.memory[ops[0]]

    def op_mul(self, ops):
        self.perform_arithmetic('__mul__', ops)

    def op_imul(self, ops):
        self.perform_arithmetic('__mul__', ops)

    def op_div(self, ops):
        op1 = self.int_from_memory(ops[0])
        op2 = self.int_from_memory(ops[1])
        a1 = ops[2]
        a2 = self.int2address(self.bits2int(ops[2]) + 1)
        self.int_to_memory(a1, op1 / op2)
        self.int_to_memory(a2, op1 % op2)

    def op_idiv(self, ops):
        op1 = self.int_from_imemory(ops[0])
        op2 = self.int_from_imemory(ops[1])
        # print op1, op2
        a1 = ops[2]
        a2 = self.int2address(self.bits2int(ops[2]) + 1)
        self.int_to_memory(a1, op1 / op2)
        self.int_to_memory(a2, op1 % op2)

    opcode_width = 2
    op_functions = {
                    '99' : 'op_halt',
                    '00' : 'op_mov',
                    '01' : 'op_add',
                    '02' : 'op_sub',
                    '13' : 'op_mul',
                    '03' : 'op_imul',
                    '04' : 'op_div',
                    '14' : 'op_idiv',
                    '80' : 'op_jmp',
                    '81' : 'op_je',
                    '82' : 'op_jne',
                    }
