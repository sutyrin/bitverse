from player import *

class BitVerse(Tickable):
    def __init__(self):
        Tickable.__init__(self)
        self.quest_givers = {}
        self.quest_recievers = {}
        
    def tick(self):
        if not keepers.getGlobalTicker().tick_number % 10:
            self.spawn_quest_giver()
        
    def run_for_ticks(self, ticks=100):
        while keepers.getGlobalTicker().tick_number < ticks:
            keepers.getGlobalTicker().tick()
            
    def spawn_quest_giver(self):
        g = QuestGiver()
        r = QuestReciever()
        g.set_reciever(r)
        r.set_giver(g)