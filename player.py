from machines import *
import keepers
import random

class Nameable:
    def __init__(self, name=None):
        if not name:
            name = ''.join(random.sample('qwertyuiopasdfghjklzxcvbnm', 5))
        self.name = name
        keepers.getGlobalKeeper().add_object(self)
        pass
    def get_name(self):
        return self.name
    def set_name(self, name):
        self.name = name
    def destroy(self):
        keepers.getGlobalKeeper().del_object_by_name(self.name)
        keepers.getGlobalTicker().del_object(self)

class Agent(Nameable):
    def __init__(self):
        Nameable.__init__(self)

class Player(Agent):
    def __init__(self):
        Agent.__init__(self)
        self.exp = 0
        self.coins = 100
        self.machine = SimpleMachine()
        self.quest = None
    def __repr__(self):
        return 'player \'%s\' exp: %d coins: %d\n%s' %\
                (self.name, self.exp, self.coins, self.machine)

    def search_and_solve_a_quest(self):
        q = keepers.getGlobalKeeper().get_object_by_class(QuestGiver)
        if q:
            self.get_quest_from(q)
            self.complete_quest_at(q.quest_reciever)
        
    def get_quest_from(self, quest_giver):
        if self.quest:
            return
        self.quest = quest_giver.get_quest(self)

    def complete_quest_at(self, quest_reciever, distort=False):
        if not self.quest:
            return

        tr = self.machine # to serve as transport

        tr.connect_to(self.quest.data_from)
        tr.recieve_datum()
        tr.disconnect()

        # keepers.getGlobalTicker().tick()  # TODO why not required?

        tr.connect_to(self.quest.data_to)
        if distort:
            tr.cell.write_value('distorted value')
        tr.transmit_datum()
        tr.disconnect()

        quest_reciever.complete_quest(self, self.quest)

class SolvingPlayer(Player, Tickable):
    def __init__(self):
        Player.__init__(self)
        Tickable.__init__(self)

    def tick(self):
        self.search_and_solve_a_quest()

class QuestGiver(Agent):
    def __init__(self):
        Agent.__init__(self)
        self.questy_value = 'questy_value'

        self.data_source = SimpleMachine()
        self.data_source.cell.write_value(self.questy_value)

    def get_quest(self, player):
        self.quest_reciever.expect_quest_value(self.questy_value)
        q = Quest(reciever=self.quest_reciever)
        q.data_from = self.data_source
        q.data_to = self.quest_reciever.data_sink
        return q

    def set_reciever(self, quest_reciever):
        self.quest_reciever = quest_reciever

class QuestReciever(Agent):
    def __init__(self):
        Agent.__init__(self)
        self.data_sink = SimpleMachine()
        self.expected_quest_value = None

    def expect_quest_value(self, value):
        self.expected_quest_value = value

    def set_giver(self, quest_giver):
        self.quest_giver = quest_giver

    def complete_quest(self, player, quest):
        if self.data_sink.cell.read_value() == self.expected_quest_value:
            player.exp += quest.exp_reward
        else:
            player.exp -= quest.exp_reward
            
        self.quest_giver.destroy()
        self.destroy()

class Quest:
    def __init__(self, reciever=None):
        self.reciever = reciever
        self.exp_reward = 1

