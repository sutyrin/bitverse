import unittest
from player import *
import keepers

class TestQuests(unittest.TestCase):
    def setUp(self):
        self.p = Player()
        self.q1 = QuestGiver()
        self.q2 = QuestReciever()
        self.q1.set_reciever(self.q2)
        self.q2.set_giver(self.q1)

    def test_simple_quest(self):
        p = self.p
        q1 = self.q1
        q2 = self.q2

        exp_before = p.exp
        p.get_quest_from(q1)
        p.complete_quest_at(p.quest.reciever)
        
        self.assertTrue(p.exp > exp_before, 'exp must raise')

    def test_failed_quest(self):
        p = self.p
        q1 = self.q1
        q2 = self.q2

        exp_before = p.exp
        p.get_quest_from(q1)
        p.complete_quest_at(p.quest.reciever, distort=True)
        
        self.assertTrue(p.exp < exp_before, 'exp must fall')
