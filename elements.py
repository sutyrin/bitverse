from keepers import Tickable
import random

import string
digs = string.digits + string.lowercase

class BitVerseException(Exception):
    pass

def ibase2int(s, base):
    res = base2int( s, base )
    if digs.index(s[0].lower()) > (base/2-1): 
        # that means 1 in binary leftmost place -- negative value 
        width = len(s) # assumingly
        res -= base ** width # make it 2-complement negative
    return res

def base2int(s, base):
    s = s.lower()
    res = 0
    for digit in s:
        res = res * base + digs.index(digit)
    return res
        
def int2base(x, base, width=0):
  if x < 0: return int2base( x + base ** width, base, width)
  elif x==0: digits = ['0']
  else: 
      digits = []
      while x:
        digits.append(digs[x % base])
        x /= base
  digits.reverse()
  digits = ['0'] * (width - len(digits)) + digits
  return ''.join(digits)

class DoublerTasker:
    def __init__(self, machine):
        self.task_input = Port()
        self.task_output = Port()
        
        self.machine = machine
        self.value = random.randrange( machine.memory.address_base ** 
                                      (machine.memory.cell_width - 1))
        self.answer = self.value * 2
        
    def prepare_input(self):
        self.task_input.write_value(self.machine.int2value(self.value))
        
    def validate(self):
        return self.machine.bits2int(self.task_output.value) == self.answer

class Memory():
    def __init__(self, address_width, cell_width, address_base = 2):
        self.address_base = address_base
        self.address_width = address_width
        self.cell_count = self.address_base ** self.address_width 
        self.cell_width = cell_width
        self.cells = dict()

    def __setitem__(self, address, value):
        value = value.strip().replace(' ', '')
        # if value != '?' or len(value) != self.cell_width:
            # raise BitVerseException('invalid value')
        self.cells[address] = value[-self.cell_width:]

    def __getitem__(self, address):
        if len(address) != self.address_width:
            raise BitVerseException('invalid address')
        if address in self.cells:
            return self.cells[address]
        else:
            return '?'*self.cell_width
        
    def set_cell_values_from_text(self, text):
        for line in text.split('\n'):
            line = line.strip()
            if line.startswith(';'):
                continue
            if line == '':
                continue
            address, value = line.split('|')
            address = address.strip()
            value = value.split(';')[0]
            #print address, value
            self[address] = value

    def __repr__(self):
        return '\n'.join(['{0}|{1}'.
          format(int2base(i, self.address_base, 
                             self.address_width), 
                 self[int2base(i, self.address_base,
                                        self.address_width)])
          for i in range(self.cell_count)][:100])

class SolarBattery(Tickable):
    def __init__(self,watts_per_tick=3):
        Tickable.__init__(self)
        self.watts_per_tick = watts_per_tick
        self.watts_available = self.watts_per_tick

    def drain(self, watts):
        """ All The drain from clients is done within tick. Next tick
        replenishes watts available (that's the solar battery). First client
        to drain is first to be served (no queue, everything is
        syncrhronous)."""
        if(self.watts_available < watts):
            #import inspect
            #print 'could not feed object:'
            #print inspect.currentframe().f_back.f_locals['self']
            return False
        self.watts_available -= watts
        return True

    def tick(self):
        self.watts_available = self.watts_per_tick

class Cell(Tickable):
    def __init__(self):
        Tickable.__init__(self)
        self.value = None
        self.generator = None
        self.name = 'c'
        self.bits = 1
        self.watts_per_tick = 1

    def __repr__(self):
        return 'cell \'%s\' of %d bits, value: \'%s\'' %\
                (self.name, self.bits, self.value)

    def read_value(self):
        v = self.value
        return v

    def write_value(self, new_value):
        self.value = new_value

    def tick(self):
        if not self.generator:
            self.value = None # value is lost if no generator connected
            return
        if not self.generator.drain(self.watts_per_tick):
            self.value = None # value is lost if insufficient power
            return

class Port(Tickable):
    def __init__(self,name='p'):
        Tickable.__init__(self)
        self.value = None
        self.name = name
        self.bits = 1
        self.plugged_port = None

    def tick(self):
        pass

    def __repr__(self):
        return 'port \'%s\' of %d bits, value: \'%s\'' %\
                (self.name, self.bits, self.value)

    def read_value(self):
        v = self.value
        self.value = None
        return v

    def write_value(self, new_value):
        if self.plugged_port:
            self.plugged_port.value = new_value
        self.value = new_value

    def plug_with_port(self, plugged_port, plug_that_part = True):
        self.plugged_port = plugged_port
        if plug_that_part:
            self.plugged_port.plug_with_port(self, False)

    def unplug(self, unplug_that_part = True):
        if self.plugged_port:
            if unplug_that_part:
                self.plugged_port.unplug(False)
            self.plugged_port = None

class Bus(Tickable):
    def __init__(self, e1, e2):
        Tickable.__init__(self)
        self.name = 'b'
        self.bits = 1
        self.e1 = e1
        self.e2 = e2
        self.watts_per_transfer = 1
        self.generator = None

    def __repr__(self):
        return 'bus \'%s\' of %d bits on [\'%s\' <==> \'%s\']' %\
                (self.name, self.bits, self.e1.name, self.e2.name)

    def tick(self):
        pass

    def send_1to2(self):
        if not self.generator:
            return
        if self.generator.drain(self.watts_per_transfer):
            self.e2.write_value(self.e1.read_value())

    def send_2to1(self):
        if not self.generator:
            return
        if self.generator.drain(self.watts_per_transfer):
            self.e1.write_value(self.e2.read_value())

