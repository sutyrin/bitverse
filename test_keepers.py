import unittest
from mock import Mock
from machines import *
import keepers
from player import Nameable

class TestKeepersReset(unittest.TestCase):
    def test_reset(self):
        keepers.getGlobalTicker().tick()
        self.assertTrue(keepers.getGlobalTicker().tick_number > 0)
        keepers.reset()
        self.assertTrue(keepers.getGlobalTicker().tick_number == 0)

class TestTickable(unittest.TestCase):
    def test_tickable_registered(self):
        class SomeTickable(Tickable):
            def __init__(self):
                Tickable.__init__(self)
            def tick(self):
                pass
        self.assertFalse(keepers.getGlobalTicker().is_registered(self))
        t = SomeTickable()
        self.assertTrue(keepers.getGlobalTicker().is_registered(t))
        
    def test_tick_propagation(self):
        class SomeTickable(Tickable):
            def __init__(self):
                Tickable.__init__(self)
            def tick(self):
                pass
        t = SomeTickable()
        t.tick = Mock()                         
        keepers.getGlobalTicker().tick()
        self.assertTrue(t.tick.call_count > 0, 'should have calls')        
        self.assertEquals(t.tick.call_count,  1, 'should have 1 calls')
    
class TestAutokeeping(unittest.TestCase):
    def setUp(self):
        keepers.reset()
        
    def test_nameable_keeping(self):
        class SomeNameable(Nameable):
            def __init__(self):
                Nameable.__init__(self)
                
        self.assertEquals(keepers.getGlobalKeeper().get_object_by_name('random name'), None,
                             'must not keep random things from the start')
        o = SomeNameable() 
        self.assertNotEquals(o.get_name(),None, 'not empty name')
        self.assertNotEquals(keepers.getGlobalKeeper().get_object_by_name(o.get_name()), None,
                             'must be already keeped')
        self.assertFalse(keepers.getGlobalTicker().is_registered(o), 
                        'must NOT be listed for ticking already')
        o.destroy()
        self.assertEquals(keepers.getGlobalKeeper().get_object_by_name(o.get_name()), None,
                             'must NOT be keeped')
        self.assertFalse(keepers.getGlobalTicker().is_registered(o), 
                        'must NOT be listed for ticking already')

    def test_tickable_autokeeping(self):
        class SomeTickable(Tickable):
            def __init__(self):
                Tickable.__init__(self)
        o = SomeTickable() 
        self.assertTrue(keepers.getGlobalTicker().is_registered(o), 
                        'must be listed for ticking already')
        o.destroy()
        self.assertFalse(keepers.getGlobalTicker().is_registered(o), 
                        'must NOT be listed for ticking already')
        
    def test_tickable_and_nameable_autokeeping(self):
        class SomeNameableTickable(Nameable,Tickable):
            def __init__(self):
                Nameable.__init__(self)
                Tickable.__init__(self)
        o = SomeNameableTickable() 
        self.assertNotEquals(keepers.getGlobalKeeper().get_object_by_name(o.get_name()), None,
                             'must be already keeped')
        self.assertTrue(keepers.getGlobalTicker().is_registered(o),
                        'must be listed for ticking already')
        o.destroy()
        self.assertEquals(keepers.getGlobalKeeper().get_object_by_name(o.get_name()), None,
                             'must NOT be keeped')
        self.assertFalse(keepers.getGlobalTicker().is_registered(o),
                        'must NOT be listed for ticking already')

        
    
