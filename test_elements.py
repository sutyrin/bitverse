import keepers
import unittest
from elements import *

class TestMemory(unittest.TestCase):
    def setUp(self):
        self.m = Memory(address_width = 2, cell_width = 4)
        
    def CONSIDER_test_invalid_value(self):
        m = self.m
        self.assertRaises(BitVerseException, m.__setitem__, '00', 'loooong')
        
    def test_invalid_address(self):
        m = self.m
        self.assertRaises(BitVerseException, m.__getitem__, 'invalid address')
        
    def test_hex(self):
        m = Memory(address_width = 4, cell_width = 14, address_base=16)
        self.assertTrue(m.address_base == 16, 'should save address base 16')
        self.assertTrue(m.cell_count == 2**16, 'should have 256 cells')
        #print m
        #self.fail()
            
    def test_set_from_text(self):
        m = self.m
        text = '\n'.join(['; some initial commentary',
                          '00|11  0  1', 
                          '',  #empty line
                          '    ' #line of spaces only
                          '01  |1  0  01 ; some comments',
                          '10|  0011 ',
                          '   11  |  1111   '])
        m.set_cell_values_from_text(text)
        
        normalized_text = '\n'.join(['00|1101', 
                                     '01|1001',
                                     '10|0011',
                                     '11|1111'])
        self.assertEquals(str(m), normalized_text)
        self.assertEquals(m['00'], '1101')
        self.assertEquals(m['01'], '1001')
        self.assertEquals(m['10'], '0011')
        self.assertEquals(m['11'], '1111')    
        
    def test_repr(self):
        m = self.m
        result = '\n'.join(['00|????', 
                            '01|????',
                            '10|????',
                            '11|????'])
        self.assertEquals(str(m), result)

        m['00'] = '1101'
        result = '\n'.join(['00|1101', 
                            '01|????',
                            '10|????',
                            '11|????'])
        self.assertEquals(str(m), result)
        
    def test_ibase2int(self):
        self.assertEquals(ibase2int('0', 2), 0)
        self.assertEquals(ibase2int('00000', 2), 0)
        self.assertEquals(ibase2int('01', 2), 1)
        self.assertEquals(ibase2int('000111', 2), 7)        
        self.assertEquals(ibase2int('11', 2), -1)
        self.assertEquals(ibase2int('11111110', 2), -2)
        self.assertEquals(ibase2int('1000000000', 2), -512)
        self.assertEquals(ibase2int('1111111111110', 2), -2)

        self.assertEquals(ibase2int('ff', 16), -1)
        self.assertEquals(ibase2int('FF', 16), -1)
                
        self.assertEquals(ibase2int('a0', 16), 10*16-256)
        self.assertEquals(ibase2int('80', 16), -128)
        self.assertEquals(ibase2int('70', 16), 7*16)
                
    def test_base2int(self):
        self.assertEquals(base2int('0', 2), 0)
        self.assertEquals(base2int('00000', 2), 0)
        self.assertEquals(base2int('01', 2), 1)
        self.assertEquals(base2int('10', 2), 2)
        self.assertEquals(base2int('100', 2), 4)
        self.assertEquals(base2int('111', 2), 7)
        self.assertEquals(base2int('000111', 2), 7)
                
    def test_simple(self):
        m = self.m
        self.assertEquals(m.cell_count, 4, 'cell count')
        self.assertEquals(m.cell_width, 4, 'cell width')
        
        self.assertEquals(m['00'], '????', 'initial cell')
        
        m['00'] = '1101'
        self.assertEquals(m['00'], '1101', 'cell assigned 1')
        m['00'] = '1001'
        self.assertEquals(m['00'], '1001', 'cell assigned 2')
            
    def test_addressing(self):
        m = self.m
        self.assertEquals(m['00'], '????', 'initial cell')
        m['00'] = '1101'
        m['01'] = '1111'
        self.assertEquals(m['00'], '1101', 'assigned cell')
        self.assertEquals(m['01'], '1111', 'assigned cell')
        
    def test_cell_overflow(self):
        m = self.m
        value = int2base(m.address_base**m.cell_width - 1, # should still fit
                         m.address_base,
                         m.address_width)
        m['00'] = value
        self.assertEquals(m['00'], value, 'assigned cell')

        value = int2base(m.address_base**m.cell_width, # should cut to 0
                         m.address_base,
                         m.address_width)
        m['00'] = value
        self.assertEquals(m['00'], '0'*m.cell_width, 'should cut')
        
    def test_int2base(self):
        self.assertEquals('001', int2base(x=1, base=2, width=3))

        
class TestCell(unittest.TestCase):
    def setUp(self):
        self.c = Cell()

    def test_simple_storage(self):
        c = self.c
        self.assertEquals(c.value, None);
        
        c.write_value('1')
        self.assertEquals(c.value, '1');
        self.assertEquals(c.read_value(), '1');
        self.assertEquals(c.read_value(), '1');
        self.assertEquals(c.read_value(), '1');  # unlimited reads

    def test_value_lost_on_no_generator(self):
        c = self.c
        c.write_value('1')
        self.assertEquals(c.read_value(), '1');
        self.assertEquals(c.read_value(), '1');
        keepers.getGlobalTicker().tick()
        self.assertEquals(c.read_value(), None);

    def test_value_lost_on_insufficient_watts(self):
        c = self.c
        c.watts_per_tick = 3
        c.generator = SolarBattery(watts_per_tick=1)
        c.write_value('1')
        self.assertEquals(c.read_value(), '1');
        keepers.getGlobalTicker().tick()
        self.assertEquals(c.read_value(), None);

    def test_value_stored_on_sufficient_watts(self):
        c = self.c
        c.watts_per_tick = 1
        c.generator = SolarBattery(watts_per_tick=1)
        c.write_value('1')
        self.assertEquals(c.read_value(), '1');
        keepers.getGlobalTicker().tick()
        self.assertEquals(c.read_value(), '1');
        keepers.getGlobalTicker().tick()
        self.assertEquals(c.read_value(), '1');

class TestSolarBattery(unittest.TestCase):
    def setUp(self):
        self.e = SolarBattery()

    def test_simple_drain(self):
        e = self.e
        self.assertEquals(e.watts_available, 3)

        self.assertTrue(e.drain(0))
        self.assertEquals(e.watts_available, 3)

        self.assertTrue(e.drain(1))
        self.assertEquals(e.watts_available, 2)
        self.assertFalse(e.drain(3))
        self.assertTrue(e.drain(2))
        self.assertEquals(e.watts_available, 0)

    def test_drain_with_tick(self):
        e = self.e
        self.assertEquals(e.watts_available, 3)
        self.assertTrue(e.drain(1))
        self.assertEquals(e.watts_available, 2)
        self.assertFalse(e.drain(3))
        self.assertTrue(e.drain(2))
        self.assertEquals(e.watts_available, 0)
        keepers.getGlobalTicker().tick()  # will replenish watts instantly
        self.assertEquals(e.watts_available, 3)
        self.assertTrue(e.drain(1))
        self.assertEquals(e.watts_available, 2)

class TestPluggedPorts(unittest.TestCase):
    def setUp(self):
        self.p1 = Port('p1')
        self.p2 = Port('p2')
    
    def test_plug_unplug(self):
        p1 = self.p1
        p2 = self.p2 

        self.assertEquals(p1.plugged_port, None);
        self.assertEquals(p2.plugged_port, None);

        self.p1.plug_with_port(self.p2)

        self.assertEquals(p1.plugged_port, p2);
        self.assertEquals(p2.plugged_port, p1);

        self.p1.unplug()

        self.assertEquals(p1.plugged_port, None);
        self.assertEquals(p2.plugged_port, None);

    def test_not_plugged(self):

        self.assertEquals(self.p1.value, None);
        self.assertEquals(self.p2.value, None);

        self.p1.write_value('1')

        self.assertEquals(self.p1.value, '1');
        self.assertEquals(self.p2.value, None);

        self.assertEquals(self.p1.read_value(), '1');

        self.assertEquals(self.p1.value, None);
        self.assertEquals(self.p2.value, None);

    def test_plugged(self):

        self.assertEquals(self.p1.value, None);
        self.assertEquals(self.p2.value, None);

        self.p1.plug_with_port(self.p2)
        self.p2.plug_with_port(self.p1)

        self.p1.write_value('1')

        self.assertEquals(self.p1.value, '1');
        self.assertEquals(self.p2.value, '1');

        self.assertEquals(self.p1.read_value(), '1');

        self.assertEquals(self.p1.value, None);
        self.assertEquals(self.p2.value, '1');

        self.assertEquals(self.p2.read_value(), '1');

        self.assertEquals(self.p1.value, None);
        self.assertEquals(self.p2.value, None);

class TestBus(unittest.TestCase):
    def setUp(self):
        self.p = Port()
        self.c = Cell()
        self.b = Bus(self.p, self.c)
        self.b.generator = SolarBattery()

    def test_simple_send1to2(self):
        self.assertEquals(self.c.value, None)

        self.p.write_value('1') 
        self.assertEquals(self.p.value, '1')
        self.assertEquals(self.c.value, None)

        self.b.send_1to2()
        self.assertEquals(self.p.value, None)
        self.assertEquals(self.c.value, '1')

    def test_simple_send2to1(self):
        self.assertEquals(self.c.value, None)

        self.c.write_value('1') 
        self.assertEquals(self.c.value, '1')

        self.b.send_2to1()
        self.assertEquals(self.p.value, '1')

class PoweredElement(Tickable):
    def __init__(self):
        Tickable.__init__(self)
        self.flag = False
        self.power_needed = {self.powered_set_flag : 1}
        self.decorate_powered_functions()
        
    def decorate_powered_functions(self):
        for f in self.power_needed:
            pass
    
    def powered_set_flag(self):
        self.flag = True
        pass

class TestPoweredFunctions(unittest.TestCase):
            
    def setUp(self):
        self.e = PoweredElement()
        self.e.generator = SolarBattery(3)

    def TODO_test_powered_function_call(self):
        was_available = self.e.generator.watts_available
        self.assertEquals(self.e.generator.watts_available, was_available)
        self.assertFalse(self.e.flag)
        self.e.powered_set_flag()
        self.assertTrue(self.e.flag)
        self.assertTrue(self.e.generator.watts_available < was_available,
                        'powered function call should drain watts')
        
if __name__ == '__main__':
    unittest.main()
