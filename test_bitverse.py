import unittest
from mock import Mock
import keepers
from player import QuestGiver, QuestReciever, Player, SolvingPlayer
from bitverse import BitVerse

from pycsp.greenlets import *

class TestTickerChannel(unittest.TestCase):
    def setUp(self):
        keepers.reset()
    def FIXME_test_simple(self):
        ticks = 0
        @process
        def ticky(tick_input, tick_counter):
            while True:
                tick_input()
                tick_counter += 1
        chan = Channel()
        Parallel(ticky(-chan, ticks))
        
class TestSimpleCreation(unittest.TestCase):
    def setUp(self):
        keepers.reset()
        self.bv = BitVerse()

    def FIXME_test_cyclic_quest_solving(self):
        bv = self.bv
        p = SolvingPlayer()
        exp_before = p.exp
        bv.run_for_ticks(100)
        self.assertTrue(p.exp > exp_before, 'player exp must raise')
        
    def test_simple_quest_solving(self):
        bv = self.bv
        p = Player()
        self.assertEquals(len(keepers.getGlobalKeeper().objects), 1, 'objects')
        bv.spawn_quest_giver()
        self.assertEquals(len(keepers.getGlobalKeeper().objects), 3, 'objects')
        exp_before = p.exp
        p.search_and_solve_a_quest()
        self.assertTrue(p.exp > exp_before, 'player exp must raise')
        self.assertEquals(len(keepers.getGlobalKeeper().objects), 1, 'objects must die')
        p.destroy()
        self.assertEquals(len(keepers.getGlobalKeeper().objects), 0, 'objects must die')
                
    def test_run_for_ticks(self):
        bv = self.bv
        self.assertNotEquals(bv, None)
        bv.run_for_ticks(ticks=100)
        self.assertEquals(keepers.getGlobalTicker().tick_number,100,
                          'current tick should be')
        #self.assertEquals(len(bv.quest_givers.keys()),100, 'givers')

    def test_spawned(self):
        bv = self.bv
        bv.spawn_quest_giver=Mock()
        bv.run_for_ticks(ticks=100)
        self.assertEquals(keepers.getGlobalTicker().tick_number,100,
                          'current tick should be')
        self.assertTrue(bv.spawn_quest_giver.call_count > 0, 
                        'should have some quester calls')
        