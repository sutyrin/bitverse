import unittest
import random
from machines import *
import keepers
from mock import Mock
from elements import *

class TestTinyMachine(unittest.TestCase):
    def setUp(self):
        self.m = TinyMachine()

    def test_simple(self):
        self.assertTrue(self.m != None)
        m = self.m
        m.memory['0'] = '1'


class TestUM3(unittest.TestCase):
    def setUp(self):
        self.m = UM3()
        
    def test_simple(self):    
        self.assertTrue(self.m.memory.cell_count == 2**16, 'memory size')
        
    def test_halt(self):
        m = self.m
        self.assertFalse(m.halted, 'should not be halted initially')
        m.memory['0000'] = '99 0000 0000 0000'
        m.step()
        self.assertTrue(m.halted, 'must be halted')

        cmd = m.current_command
        m.step()
        self.assertEquals(m.current_command, cmd, 'must not step while halted')

    def test_mov(self):
        m = self.m
        m.memory.set_cell_values_from_text("""
        ;   adr  | opc op1 op2 op3   ; comment
        ; -------+-------------------;------------
            0000 | 00 0100 0000 0101 ; y := x
            0001 | 99 0000 0000 0000 ; halt
            0100 | 00 0000 0000 0001 ; x   
            0101 | 00 0000 0000 0000 ; y   
        """)
        #print m.memory
        while not m.halted:
            m.step()
        self.assertTrue(m.halted, 'halted')
        self.assertEquals(m.memory['0101'], '00000000000001')

    def test_add(self):
        m = self.m
        m.memory.set_cell_values_from_text("""
        ;   adr  | opc op1 op2 op3   ; comment
        ; -------+-------------------;------------
            0000 | 01 0100 0101 0101 ; y := x + y
            0001 | 99 0000 0000 0000 ; halt
            0100 | 00 0000 0000 0001 ; x   
            0101 | 00 0000 0000 0010 ; y   
        """)
        #print m.memory
        while not m.halted:
            m.step()
        self.assertTrue(m.halted, 'halted')
        self.assertEquals(m.memory['0101'], '00000000000011')

    def test_sub(self):
        m = self.m
        m.memory.set_cell_values_from_text("""
        ;   adr  | opc op1 op2 op3   ; comment
        ; -------+-------------------;------------
            0000 | 02 0100 0101 0101 ; y := x + y
            0001 | 99 0000 0000 0000 ; halt
            0100 | 00 0000 0000 0011 ; x   
            0101 | 00 0000 0000 0001 ; y   
        """)
        #print m.memory
        while not m.halted:
            m.step()
        self.assertTrue(m.halted, 'halted')
        self.assertEquals(m.memory['0101'], '00000000000010')
        
    def test_mul(self):
        m = self.m
        m.memory.set_cell_values_from_text("""
        ;   adr  | opc op1 op2 op3   ; comment
        ; -------+-------------------;------------
            0000 | 13 0100 0101 0101 ; y := x mul y
            0001 | 99 0000 0000 0000 ; halt
            0100 | 00 0000 0000 0100 ; x   
            0101 | 00 0000 0000 0011 ; y   
        """)
        #print m.memory
        while not m.halted:
            m.step()
        self.assertTrue(m.halted, 'halted')
        self.assertEquals(m.memory['0101'], '00000000001100')
        
    def test_imul(self):
        m = self.m
        m.memory.set_cell_values_from_text("""
        ;   adr  | opc op1 op2 op3   ; comment
        ; -------+-------------------;------------
            0000 | 03 0100 0101 0101 ; y := x imul y
            0001 | 99 0000 0000 0000 ; halt
            0100 | 00 0000 0000 0002 ; x   
            0101 | ff ffff ffff ffff ; y   
        """)
        #print m.memory
        while not m.halted:
            m.step()
        self.assertTrue(m.halted, 'halted')
        self.assertEquals(m.memory['0101'], 'fffffffffffffe')
        
    def test_div(self):
        m = self.m
        m.memory.set_cell_values_from_text("""
        ;   adr  | opc op1 op2 op3   ; comment
        ; -------+-------------------;------------
            0000 | 04 0100 0101 0100 ; (x,y) := x div y
            0001 | 99 0000 0000 0000 ; halt
            0100 | 00 0000 0000 0101 ; x   
            0101 | 00 0000 0000 0010 ; y   
        """)
        #print m.memory
        while not m.halted:
            m.step()
        self.assertTrue(m.halted, 'halted')
        self.assertEquals(m.memory['0100'], '00000000000010')
        self.assertEquals(m.memory['0101'], '00000000000001')
    
    def test_idiv(self):
        m = self.m
        # >>> divmod(5,2) # (/,%)
        # (2, 1)
        # >>> divmod(-5,2)
        # (-3, 1)
        # >>> divmod(-5,-2)
        # (2, -1)
        # >>> divmod(5,-2)
        # (-3, -1)

        m.memory.set_cell_values_from_text("""
        ;   adr  | opc op1 op2 op3   ; comment
        ; -------+-------------------;------------
            0000 | 14 0100 0101 0100 ; (x,y) := x idiv y
            0001 | 99 0000 0000 0000 ; halt
            0100 | 00 0000 0000 0005 ; x = 5  
            0101 | ff ffff ffff fffe ; y = -2  
        """)
        #print m.memory
        m.step_to_halt()
        self.assertTrue(m.halted, 'halted')
        self.assertEquals(m.memory['0100'], 'fffffffffffffd') # -3
        self.assertEquals(m.memory['0101'], 'ffffffffffffff') # -1
        
    def test_jmp(self):
        m = self.m
        m.memory.set_cell_values_from_text("""
        ;   adr  | opc op1 op2 op3   ; comment
        ; -------+-------------------;------------
            0000 | 80 0000 0000 0002 ; jmp 0002
            0001 | 99 0000 0000 0000 ; halt
            0002 | 99 0000 0000 0000 ; halt  
        """)
        m.step()
        m.step()
        self.assertEquals(m.current_command, 2, 'must have jumped')

    def test_je(self):
        m = self.m
        m.memory.set_cell_values_from_text("""
        ;   adr  | opc op1 op2 op3   ; comment
        ; -------+-------------------;------------
            0000 | 81 0101 0102 0002 ; if x == y then jmp 0002
            0001 | 99 0000 0000 0000 ; halt
            0002 | 99 0000 0000 0000 ; halt
            0101 | 00 0000 0000 0010 ; x = 2 
            0102 | 00 0000 0000 0010 ; y = 2  
        """)
        m.step()
        m.step()
        self.assertEquals(m.current_command, 2, 'must have jumped')
        
    def test_jne(self):
        m = self.m
        m.memory.set_cell_values_from_text("""
        ;   adr  | opc op1 op2 op3   ; comment
        ; -------+-------------------;------------
            0000 | 82 0101 0102 0002 ; if x == y then jmp 0002
            0001 | 99 0000 0000 0000 ; halt
            0002 | 99 0000 0000 0000 ; halt
            0101 | 00 0000 0000 0010 ; x = 2 
            0102 | 00 0000 0000 0011 ; y = 3  
        """)
        m.step()
        m.step()
        self.assertEquals(m.current_command, 2, 'must have jumped')

class TestThreeAddressMachine(unittest.TestCase):
    def setUp(self):
        self.address_width=3
        self.opcode_width=3
        self.mem = Memory(address_width=self.address_width, 
                     cell_width=self.opcode_width + 3 * self.address_width)
        self.m = ThreeAddressMachine(self.mem)
        
    def test_simple(self):
        m = self.m
        mem = self.mem
        self.assertEquals(m.memory, mem, 'initial memory')
        
    def test_halt(self):
        m = self.m
        self.assertFalse(m.halted, 'should not be halted initially')
        m.memory['000'] = '111 000 000 000'
        #print m.memory
        m.step()
        self.assertTrue(m.halted, 'must halt')

        cmd = m.current_command
        m.step()
        self.assertEquals(m.current_command, cmd, 'must not step while halted')
        
    def test_add(self):
        m = self.m
        m.memory.set_cell_values_from_text("""
        ;   adr | opc op1 op2 op3 ; comment
        ; ------+-----------------;------------
            000 | 001 100 111 100 ; x = x + y
            001 | 111 000 000 000 ; halt
            100 | 000 000 000 011 ; x 
            111 | 000 000 000 100 ; y
        """)
        #print m.memory
        while not m.halted:
            m.step()
        self.assertTrue(m.halted, 'halted')
        self.assertEquals(m.memory['100'], '000000000111')
        
    def test_sub(self):
        m = self.m
        m.memory.set_cell_values_from_text("""
        ;   adr | opc op1 op2 op3 ; comment
        ; ------+-----------------;------------
            000 | 010 100 111 100 ; x = x - y
            001 | 111 000 000 000 ; halt
            100 | 000 000 000 111 ; x 
            111 | 000 000 000 010 ; y
        """)
        #print m.memory
        while not m.halted:
            m.step()
        self.assertEquals(m.memory['100'], '000000000101')
        self.assertTrue(m.halted, 'halted')
        
    def test_outport(self):
        m = self.m
        m.ports[1].write_value = Mock()
        m.memory.set_cell_values_from_text("""
        ;   adr | opc op1 op2 op3 ; comment
        ; ------+-----------------;------------
            000 | 101 100 000 000 ; send x to port 000
            001 | 101 000 000 000 ; send [000] to port 000
            010 | 111 000 000 000 ; halt
            100 | 000 000 010 111 ; x 
        """)
        #print m.memory
        while not m.halted:
            m.step()
        self.assertTrue(m.halted, 'halted')
        self.assertTrue(m.ports[1].write_value.call_count == 2, 
                        'port must be written twice')
        self.assertEquals(m.ports[1].write_value.call_args_list,
                          [(('000000010111',), {}), 
                           (('101100000000',), {})])

    def FIXME_test_inport(self):
        m = self.m
        m.ports[0].read_value = Mock()
        m.ports[0].read_value.side_effect = lambda: '010010010010';
        m.memory.set_cell_values_from_text("""
        ;   adr | opc op1 op2 op3 ; comment
        ; ------+-----------------;------------
            000 | 100 000 000 100 ; read x from port 000
            001 | 100 000 000 101 ; read y from port 000
            010 | 111 000 000 000 ; halt
            100 | 000 000 000 000 ; x 
            101 | 000 000 000 000 ; y 
        """)
        #print m.memory
        m.ports[0].read_value.side_effect = lambda: '010010010010';
        m.step()
        m.ports[0].read_value.side_effect = lambda: '100100100100';
        m.step()
        m.step()
        self.assertTrue(m.halted, 'must be halted')
        self.assertTrue(m.ports[0].read_value.call_count == 2, 
                        'port must be read twice')
        self.assertEquals(m.memory['100'], '010010010010')
        self.assertEquals(m.memory['101'], '100100100100')
        
    def test_jmp(self):
        m = self.m
        m.memory.set_cell_values_from_text("""
        ;   adr | opc op1 op2 op3 ; comment
        ; ------+-----------------;------------
            000 | 110 000 000 010 ; jmp 010
            001 | 111 000 000 101 ; halt   
            010 | 111 000 000 000 ; halt 
        """)
        m.step()
        m.step()
        self.assertEquals(m.current_command, 2, 'must jump')
        
    def test_jne(self):
        m = self.m

        #WRONG, but could work later        
        m.memory.set_cell_values_from_text("""
        ;   adr | opc op1 op2 op3 ; comment
        ; ------+-----------------;------------
            000 | 100 000 000 100 ; read x from port 000
            001 | 011 100 101 100 ; if x <> y then goto 005
            010 | 101 110 000 000 ; write 1 to port 000
            011 | 110 000 000 101 ; jmp end
            100 | 101 111 000 000 ; write 0 to port 000
            101 | 111 000 000 000 ; end: halt
            110 | 000 000 000 001 ; 1 
            111 | 000 000 000 000 ; 0 
        """)

        m.memory.set_cell_values_from_text("""
        ;   adr | opc op1 op2 op3 ; comment
        ; ------+-----------------;------------
            000 | 100 000 000 100 ; read x from port 000
            001 | 011 100 101 011 ; if x <> 0 then goto 011
            010 | 111 000 000 000 ; halt
            011 | 111 000 000 000 ; halt
            101 | 000 000 000 000 ; 0
        """)
        m.ports[0].read_value = Mock(side_effect = lambda: '0000000001');
        self.assertFalse(m.halted, 'must not be halted')
        m.step(3)
        self.assertTrue(m.halted, 'must halt')
        self.assertEquals(m.current_command, 3, 'should jump if x ne 0')
        
        m.ports[0].read_value = Mock(side_effect = lambda: '0000000000');
        m.reset()
        self.assertFalse(m.halted, 'must not be halted')
        m.step(3)
        self.assertTrue(m.halted, 'must halt')
        self.assertEquals(m.current_command, 2, 'should not jump if x == 0')

    def test_task_doubler(self):
        m = self.m
        m.memory.set_cell_values_from_text("""
        ;   adr | opc op1 op2 op3 ; comment
        ; ------+-----------------;------------
            000 | 100 000 000 100 ; read x from port 000
            001 | 001 100 100 100 ; x = x + x
            010 | 101 100 000 000 ; write x to port 000
            011 | 111 000 000 000 ; halt
        """)
        input = 18  #random.randrange(1024)
        output = 2 * input 
        m.ports[0].read_value = Mock(side_effect = lambda: m.int2value(input));
        m.ports[1].write_value = Mock()
        m.step(4)
        self.assertTrue(m.halted, 'must halt')
        self.assertEquals(m.ports[1].write_value.call_args_list,
                          [((m.int2value(output),), {}),]) 
    def test_task_doubler_gen(self):
        m = self.m
        m.memory.set_cell_values_from_text("""
        ;   adr | opc op1 op2 op3 ; comment
        ; ------+-----------------;------------
            000 | 100 000 000 100 ; read x from port 000
            001 | 001 100 100 100 ; x = x + x
            010 | 101 100 000 000 ; write x to port 000
            011 | 111 000 000 000 ; halt
        """)
        t = DoublerTasker(m)
        m.ports[0].plug_with_port( t.task_input )
        m.ports[1].plug_with_port( t.task_output )
        t.prepare_input()
        m.step_to_halt() #TODO refactor other whiles to this call
        self.assertTrue(t.validate(), 'must validate')

class TestThreeMachineTransfer(unittest.TestCase):
    def setUp(self):
        self.m1 = SimpleMachine()
        self.m2 = SimpleMachine()
        self.tr = SimpleMachine()
        self.m1.cell.write_value('1')

    def test_three_machine_transfer(self):
        """ transfer a datum from m1 to m2 via tr """
        m1 = self.m1
        m2 = self.m2
        tr = self.tr

        self.assertEquals(m1.cell.read_value(), '1')
        self.assertEquals(m2.cell.read_value(), None)

        tr.connect_to(m1)
        tr.recieve_datum()
        tr.disconnect()

        keepers.getGlobalTicker().tick()

        tr.connect_to(m2)
        tr.transmit_datum()
        tr.disconnect()

        self.assertEquals(m1.cell.read_value(), '1')
        self.assertEquals(m2.cell.read_value(), '1')

class TestInterMachineTransfer(unittest.TestCase):
    def setUp(self):
        self.m1 = SimpleMachine()
        self.m2 = SimpleMachine()
        self.m1.port.plug_with_port(self.m2.port)

    def test_transfer(self):
        m1 = self.m1
        m2 = self.m2
        self.assertEquals(m1.port.value, None)
        self.assertEquals(m2.port.value, None)
        self.assertEquals(m1.cell.value, None)
        self.assertEquals(m2.cell.value, None)

        m1.cell.write_value('1')
        self.assertEquals(m2.cell.value, None)

        m1.cmd_cell_to_port()
        self.assertEquals(m2.port.value, '1')
        self.assertEquals(m2.cell.value, None)

        m2.cmd_port_to_cell()
        self.assertEquals(m2.port.value, None)
        self.assertEquals(m2.cell.value, '1')

        self.assertEquals(m1.cell.value, '1')
        self.assertEquals(m2.cell.value, '1')

class TestCellToPortTransfer(unittest.TestCase):
    def setUp(self):
        self.m = SimpleMachine()

    def test_cell_to_port_transfer(self):
        m = self.m
        m.cell.write_value('1')
        self.assertEquals(m.cell.read_value(), '1')
        self.assertEquals(m.cell.read_value(), '1') # unexpendable

        self.assertEquals(m.port.read_value(), None)
        self.assertEquals(m.port.read_value(), None) # empty port

        m.cmd_cell_to_port()

        self.assertEquals(m.cell.read_value(), '1')
        self.assertEquals(m.port.read_value(), '1')
        self.assertEquals(m.port.read_value(), None)

if __name__ == '__main__':
    unittest.main()

